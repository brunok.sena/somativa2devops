FROM nginx

RUN apt-get update
RUN apt-get install nginx -y

COPY somativa2.html /usr/share/nginx/html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
